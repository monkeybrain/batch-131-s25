﻿
db.fruits.insertMany([
{"name": "Banana", 
"supplier": "Farmer Fruits Inc.", 
"stocks": "30", 
"price": "20", 
"onSale": true},

{"name": "Grapes", 
"supplier": "Fruity Co.", 
"stocks": "30", 
"price": "100", 
"onSale": true},

{"name": "Mango", 
"supplier": "Mango Magic Inc.", 
"stocks": "50", 
"price": "70",
"onSale": true},

{"name": "Apple", 
"supplier": "Apple Valley", 
"stocks": "0", 
"price": "20",
"onSale": false},

{"name": "Dragon Fruit", 
"supplier": "Farmer Fruits Inc.", 
"stocks": "10", 
"price": "60",
"onSale": true},

{"name": "Papaya", 
"supplier": "Fruity Co.", 
"stocks": "15", 
"price": "60", 
"onSale": true}
]);


//task2
   db.fruits.aggregate([
            {$match: {onSale: true}},    
            {$count: "totalNumberofFruitsonSale"}
        ])

//task 3   
  db.fruits.aggregate([
            {$match: {"stocks": {$gt: 20}},    
            {$count: "totalNumberofFruits with stocks >20"}
        ]);

//task 4
db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: {_id: "$supplier", avg_price: {$avg: "$price"}} }
 ]);


//task 5
db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: {_id: "$supplier", max_price: {$max: "$price"}} }
 ]);

//task 6
db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: {_id: "$supplier", min_price: {$min: "$price"}} }
 ]);